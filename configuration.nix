{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  # Bootloader
  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
    useOSProber = true;
  };

  # Networking
  networking.hostName = "iot";
  networking.networkmanager.enable = true;

  # Time zone
  time.timeZone = "Europe/Copenhagen";

  # Internationalisation properties
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "da_DK.UTF-8";
    LC_IDENTIFICATION = "da_DK.UTF-8";
    LC_MEASUREMENT = "da_DK.UTF-8";
    LC_MONETARY = "da_DK.UTF-8";
    LC_NAME = "da_DK.UTF-8";
    LC_NUMERIC = "da_DK.UTF-8";
    LC_PAPER = "da_DK.UTF-8";
    LC_TELEPHONE = "da_DK.UTF-8";
    LC_TIME = "da_DK.UTF-8";
  };

  # Users
  users.users.admin = {
    isNormalUser = true;
    description = "admin";
    extraGroups = ["networkmanager" "wheel" "dialout"];
  };

  # Packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    helix
    git
    go
    zellij
  ];

  # Services
  services = {
    # Prometheus used to collect data for Grafana
    prometheus = {
      enable = true;

      # Server setup
      globalConfig.scrape_interval = "10s";
      scrapeConfigs = [
        {
          job_name = "node";
          static_configs = [
            {
              targets = ["localhost:${toString config.services.prometheus.exporters.node.port}"];
            }
          ];
        }
      ];

      # Node setup
      exporters.node = {
        enable = true;
        port = 9000;
        enabledCollectors = ["systemd"];
        extraFlags = ["--collector.ethtool" "--collector.softirqs" "--collector.tcpstat" "--collector.wifi"];
      };
    };

    # Grafana
    grafana = {
      enable = true;
      settings = {
        server = {
          http_addr = "0.0.0.0";
          http_port = 3000;
          serve_from_sub_path = true;
        };
      };
    };

    # Samba
    samba = {
      enable = true;
      securityType = "user";
      openFirewall = true;
      extraConfig = ''
        workgroup = WORKGROUP
        server string = smbnix
        netbios name = smbnix
        security = user
        hosts allow = 10.108.149. 127.0.0.1 localhost
        hosts deny = 0.0.0.0/0
        guest account = nobody
        map to guest = bad user
      '';
      shares = {
        csv = {
          path = "/home/admin/enviro-csv";
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "no";
          "validUsers" = ["admin"];
          "create mask" = "0644";
          "directory mask" = "0755";
        };
      };
    };

    samba-wsdd = {
      enable = true;
      openFirewall = true;
    };

    # Enable SSH
    openssh.enable = true;

    # Keymap for X11
    xserver = {
      layout = "us";
      xkbVariant = "";
    };
  };

  # Shell
  programs = {
    fish.enable = true;
    starship.enable = true;
  };

  # Firewall open ports
  networking.firewall = {
    enable = false;
    allowedTCPPorts = [8080 443 80 4200 22];
    allowPing = true;
  };

  # DO NOT CHANGE.
  system.stateVersion = "23.11";
}
