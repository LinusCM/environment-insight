# Endpoints
### *Last updated: May 7th, 2024*

A collection of endpoints supplied by the Go REST API.
Contains basic descriptions and in/out JSON's.

#### Endpoints:
- POST `/login`
- GET `/clients`
- GET `/clients/<client id>`
- POST `/clients/<client id>`
- POST `/clients/<client id>/data`

## **POST** `/login`
Login for API, returns a JWT.

### Takes:
```json
{
    "username": "username",
    "password": "password"
}
```

### Returns:
```json
{
    "token": "eyJhbGciOiJIHMzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAasdiOjE3MTQzgh3MTB9.s_fchgYoqIYKVgMfoyaccP1DbtfLSDN9qJegIWEqHOA"
}
```

## **GET** `/clients`
Get all available clients and their location.

### Takes:
Nothing.

### Returns:
```json
[
    {
        "ID": "111",
        "Institution": "Institution",
        "Room": "Room"
    },
    {
        "ID": "112",
        "Institution": "",
        "Room": ""
    }
]
```

## **GET** `/clients/<client id>`
Get a clients location.

### Takes:
Nothing.

### Returns:
```json
{
    "ID": "111",
    "Institution": "Institution",
    "Room": "Room"
},
```

## **POST** `/clients/<client id>`
Update a clients location.

### Takes:
```json
{
    "institution": "Institution",
    "room": "Room"
}
```

### Returns:
```json
{
    "ID": "112",
    "institution": "Institution",
    "room": "Room"
}
```

## **GET** `/clients/<client id>/data`
Fetches sensor data from a client.
Max timespan for a fetch is 1 year.

### Takes:
```json
{    
    "start_time": 1714034160,
    "end_time": 1714034170
}
```

### Returns:
```json
{
    [
        {
            "id": "test",
            "timestamp": 1714034162,
            "light": 1000,
            "temperature": 100,
            "humidity": 10,
            "pressure": 50,
            "altitude": 40
        },
        {
            "id": "test",
            "timestamp": 1714034163,
            "light": 1000,
            "temperature": 100,
            "humidity": 10,
            "pressure": 50,
            "altitude": 40
        },
    ]
}
```
