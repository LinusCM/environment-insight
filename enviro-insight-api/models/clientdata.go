package models

type ClientData struct {
	ClientID    string `json:"client_id" gorm:"primaryKey" gorm:"NOT NULL"`
	Timestamp   int64  `json:"timestamp" gorm:"primaryKey" gorm:"NOT NULL"`
	Light       int    `json:"light"`
	Temperature int    `json:"temperature"`
	Humidity    int    `json:"humidity"`
	Pressure    int    `json:"pressure"`
	Altitude    int    `json:"altitude"`
}
