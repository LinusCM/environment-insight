package models

type Client struct {
	ID          string       `gorm:"primaryKey" gorm:"NOT NULL"`
	Institution string       `gorm:"nullable"`
	Room        string       `gorm:"nullable"`
	ClientData  []ClientData `gorm:"foreignKey:ClientID" json:"-"`
}
