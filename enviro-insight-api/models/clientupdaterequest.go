package models

type ClientUpdateRequest struct {
	Institution string `json:"institution"`
	Room        string `json:"room"`
}
