package models

type Config struct {
	RabbitMQ struct {
		IP       string `json:"ip"`
		Queue    string `json:"queue"`
		Username string `json:"username"`
		Password string `json:"password"`
		Port     string `json:"port"`
	} `json:"rabbitMQ"`
	User struct {
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"user"`
	DB struct {
		IP       string `json:"ip"`
		Port     string `json:"port"`
		Name     string `json:"name"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"db"`
}
