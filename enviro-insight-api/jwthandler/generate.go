package jwthandler

import (
	"log"
	"time"

	"enviro-insight-api/models"

	"github.com/golang-jwt/jwt/v5"
)

// WARNING: This is a temp solution, just to get things working.
// You should never store keys in your code.
var key = []byte("ThisIsAREALLYGoodKEy")

func Generate(username string, expirationHours int) string {
	// Set expiration date in hours
	expiresAt := time.Now().Add(time.Hour * time.Duration(expirationHours)).Unix()

	// Token setup
	payload := models.Claims{
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Unix(expiresAt, 0)),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)

	// We never want signing to fail
	tokenString, err := token.SignedString(key)
	if err != nil {
		log.Fatal(err)
	}

	return tokenString
}
