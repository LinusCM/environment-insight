package jwthandler

import (
	"errors"
	"time"

	"enviro-insight-api/models"

	"github.com/golang-jwt/jwt/v5"
)

func Authenticate(tokenString string) (models.Claims, error) {
	// jwt.Parse takes two arguments, the latter being an anonymous function
	// the tokenString gets parsed and tossed into the anonymous function.
	// In the anonymous function, the key is found.
	// We don't care, as we have one key and that one gets returned.
	token, err := jwt.ParseWithClaims(tokenString, &models.Claims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return *new(models.Claims), errors.New("Can't parse given token.")
	}

	// Verify if the token is valid
	if claims, ok := token.Claims.(*models.Claims); ok && token.Valid {
		// Check if token has expired
		if claims.ExpiresAt != nil && time.Now().Unix() > claims.ExpiresAt.Unix() {
			return models.Claims{}, errors.New("Token has expired.")
		}

		// Token is valid and not expired
		return *claims, nil
	}

	// Token/payload was invalid in some other way
	return *new(models.Claims), errors.New("Payload and/or token is invalid.")
}
