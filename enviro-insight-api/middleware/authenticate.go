package middleware

import (
	"net/http"

	"enviro-insight-api/requests"

	"github.com/gin-gonic/gin"
)

func Auth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		_, err := requests.Authenticate(ctx)
		if err != nil {
			ctx.JSON(http.StatusUnauthorized, gin.H{})
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}
