package rabbitmq

import (
	"log"

	"enviro-insight-api/models"

	amqp "github.com/rabbitmq/amqp091-go"
)

func Setup(config models.Config) (*amqp.Channel, *amqp.Connection) {
	// Establish connection to RabbitMQ server
	conn, err := amqp.Dial("amqp://" + config.RabbitMQ.Username + ":" + config.RabbitMQ.Password + "@" + config.RabbitMQ.IP + ":" + config.RabbitMQ.Port + "/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}

	// Open a channel
	channel, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}

	return channel, conn
}
