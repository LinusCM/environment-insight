package rabbitmq

import (
	"encoding/json"
	"enviro-insight-api/models"
	"log"
	"strconv"

	amqp "github.com/rabbitmq/amqp091-go"
	"gorm.io/gorm"
)

func StoreMessages(ch *amqp.Channel, queueName string, db *gorm.DB) {
	msgs, err := ch.Consume(
		queueName,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	// Process incoming messages
	for msg := range msgs {
		var clientData models.ClientData

		// Unescape the JSON string before unmarshaling
		unescapedMsgBody, err := strconv.Unquote(string(msg.Body))
		if err != nil {
			log.Printf("Error unescaping JSON: %v\n", err)
			continue
		}

		// Unmarshal the unescaped JSON string
		if err := json.Unmarshal([]byte(unescapedMsgBody), &clientData); err != nil {
			log.Printf("Error parsing JSON: %v\n", err)
			continue
		}

		// Retrieve the client from the database based on ClientID
		var client models.Client
		if err := db.FirstOrCreate(&client, models.Client{ID: clientData.ClientID}).Error; err != nil {
			log.Printf("Error finding or creating client: %v\n", err)
			continue
		}

		// Associate the client with the sensor data
		clientData.ClientID = client.ID

		// Create the sensor data
		if err := db.Create(&clientData).Error; err != nil {
			log.Printf("Error creating sensor data: %v\n", err)
			continue
		}
	}
}
