package main

import (
	"log"

	"enviro-insight-api/database"
	"enviro-insight-api/middleware"
	"enviro-insight-api/rabbitmq"
	"enviro-insight-api/requests"

	"github.com/gin-gonic/gin"
)

func main() {
	////////////////////
	// Config
	////////////////////
	config, err := fetchConfig()
	if err != nil {
		log.Fatalf("Failed to parse config file: %v", err)
	}

	////////////////////
	// Database
	////////////////////
	db := database.Setup(config)

	////////////////////
	// RabbitMQ
	////////////////////
	channel, conn := rabbitmq.Setup(config)
	defer conn.Close()
	defer channel.Close()

	// Start a goroutine to consume messages from queue
	go rabbitmq.StoreMessages(channel, config.RabbitMQ.Queue, db)

	////////////////////
	// REST API
	////////////////////
	router := gin.Default()
	router.Use(middleware.Cors())

	// Login for JWT
	router.POST("/login", func(ctx *gin.Context) {
		requests.Login(ctx, config)
	})

	// Authenticated "clients" endpoints
	authenticated := router.Group("/clients")
	authenticated.Use(middleware.Auth())

	// Fetch client locals
	authenticated.GET("", func(ctx *gin.Context) {
		requests.FetchClients(ctx, db)
	})

	// Fetch client local
	authenticated.GET("/:client_name", func(ctx *gin.Context) {
		requests.FetchClient(ctx, db)
	})

	// Fetch client data over time period
	authenticated.POST("/:client_name/data", func(ctx *gin.Context) {
		requests.FetchClientData(ctx, db)
	})

	// Update client local
	authenticated.POST("/:client_name", func(ctx *gin.Context) {
		requests.UpdateClient(ctx, db)
	})

	// Start REST API
	router.Run("0.0.0.0:8080")
}
