package database

import (
	"log"

	"enviro-insight-api/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Setup(config models.Config) *gorm.DB {
	dsn := "host=" + config.DB.IP + " user=" + config.DB.Username + " password=" + config.DB.Password + " dbname=" + config.DB.Name + " port=" + config.DB.Port + " sslmode=disable TimeZone=UTC"

	// Open a connection to the PostgreSQL database
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Could not connect to database: %v", err)
	}

	// Migrate the schema
	db.AutoMigrate(&models.Client{}, &models.ClientData{})

	return db
}
