package requests

import (
	"errors"
	"net/http"

	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func FetchClient(ctx *gin.Context, db *gorm.DB) {
	// Find client
	clientName := ctx.Param("client_name")

	var client models.Client
	if err := db.Model(models.Client{ID: clientName}).First(&client).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
		return
	}

	ctx.JSON(http.StatusOK, client)
}
