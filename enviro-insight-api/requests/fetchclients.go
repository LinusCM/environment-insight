package requests

import (
	"errors"
	"net/http"

	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func FetchClients(ctx *gin.Context, db *gorm.DB) {

	var clients []models.Client
	if err := db.Select("id, institution, room").Find(&clients).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
		return
	}

	ctx.JSON(http.StatusOK, clients)
}
