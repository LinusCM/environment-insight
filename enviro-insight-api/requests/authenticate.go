package requests

import (
	"errors"
	"net/http"
	"strings"

	"enviro-insight-api/jwthandler"
	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
)

func Authenticate(ctx *gin.Context) (string, error) {
	var payload models.Claims

	// Check validity of header
	auth := ctx.Request.Header["Authorization"]

	//Header wrong
	if len(auth) < 1 {
		ctx.JSON(http.StatusBadRequest, gin.H{})
		return "", errors.New("No auth header passed.")
	}

	// Check auth scheme
	authParts := strings.Split(auth[0], " ")

	if authParts[0] != "Bearer" || len(authParts) != 2 {
		ctx.JSON(http.StatusBadRequest, gin.H{})
		return "", errors.New("Auth scheme is invalid.")
	}

	// Auth wrong
	payload, err := jwthandler.Authenticate(authParts[1])
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{})
		return payload.Subject, errors.New("Auth is invalid.")
	}

	return payload.Subject, nil
}
