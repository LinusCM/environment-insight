package requests

import (
	"errors"
	"net/http"

	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func UpdateClient(ctx *gin.Context, db *gorm.DB) {
	// Get client name from path parameters
	clientName := ctx.Param("client_name")

	// Parse JSON request body into ClientUpdateRequest struct
	var updateRequest models.ClientUpdateRequest
	if err := ctx.ShouldBindJSON(&updateRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	// Query the database
	var client models.Client
	if err := db.Where("id = ?", clientName).First(&client).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
		return
	}

	// Update the client fields with the values from the request
	client.Institution = updateRequest.Institution
	client.Room = updateRequest.Room

	// Save the updated client to the database
	if err := db.Save(&client).Error; err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
		return
	}

	// If client updated successfully, return it in the response
	ctx.JSON(http.StatusOK, client)
}
