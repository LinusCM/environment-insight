package requests

import (
	"encoding/json"
	"net/http"

	"enviro-insight-api/jwthandler"
	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
)

func Login(ctx *gin.Context, config models.Config) {
	// Get username/password from request body.
	user := models.LoginRequest{
		Username: ctx.PostForm("username"),
		Password: ctx.PostForm("password"),
	}

	var loginRequest models.LoginRequest
	if err := json.NewDecoder(ctx.Request.Body).Decode(&loginRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{})
		return
	}

	// Validate credentials
	if config.User.Username != loginRequest.Username || config.User.Password != loginRequest.Password {
		ctx.JSON(http.StatusUnauthorized, gin.H{})
		return
	}

	// Return token
	ctx.JSON(http.StatusOK, gin.H{
		"token": jwthandler.Generate(user.Username, 1),
	})
}
