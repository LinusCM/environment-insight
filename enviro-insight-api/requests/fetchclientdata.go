package requests

import (
	"errors"
	"net/http"

	"enviro-insight-api/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func FetchClientData(ctx *gin.Context, db *gorm.DB) {
	// Map body to EntryRequest model
	var entryRequest models.EntryRequest
	if err := ctx.BindJSON(&entryRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	// Find client
	clientName := ctx.Param("client_name")

	var client models.Client
	if err := db.Preload("ClientData").Where("id = ?", clientName).First(&client).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
		return

	}

	// Max response can be 1 year, to avoid performance issues
	var yearInSeconds int64 = 31536000
	requestPeriod := entryRequest.EndTime - entryRequest.StartTime

	// Fetch the entries within the given time range, if its valid
	var clientDataInRange []models.ClientData
	for _, data := range client.ClientData {
		if requestPeriod > 0 && requestPeriod < yearInSeconds {
			if data.Timestamp >= entryRequest.StartTime && data.Timestamp <= entryRequest.EndTime {
				clientDataInRange = append(clientDataInRange, data)
			}
		}
	}

	// Return the queried data as JSON response
	ctx.JSON(http.StatusOK, clientDataInRange)
}
