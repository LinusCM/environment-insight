package main

import (
	"encoding/json"
	"enviro-insight-api/models"
	"os"
)

func fetchConfig() (models.Config, error) {
	var config models.Config

	file, err := os.Open("./config.json")
	if err != nil {
		return config, err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		return config, err
	}

	return config, nil
}
