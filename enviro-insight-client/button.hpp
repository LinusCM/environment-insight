const unsigned int buttonOne = 5;
const unsigned int buttonTwo = 4;
uint8_t buttonState = 0;

class ButtonHandler {
private:
  uint8_t buttonOne, buttonTwo, maxPages;
  unsigned long lastButtonPress = 0;

public:
  ButtonHandler(uint8_t buttonOneInit, uint8_t buttonTwoInit,
                uint8_t maxPagesInit)
      : buttonOne(buttonOneInit), buttonTwo(buttonTwoInit),
        maxPages(maxPagesInit) {}

  void checkButtons(unsigned long currentMillis) {
    if (digitalRead(buttonOne) == HIGH &&
        currentMillis - 200 > lastButtonPress) {
      lastButtonPress = currentMillis;
      buttonState = buttonState == 0 ? maxPages : buttonState - 1;
    }

    if (digitalRead(buttonTwo) == HIGH &&
        currentMillis - 200 > lastButtonPress) {
      lastButtonPress = currentMillis;
      buttonState = buttonState == maxPages ? 0 : buttonState + 1;
    }
  }
};
