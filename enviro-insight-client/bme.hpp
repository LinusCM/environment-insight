#include <Adafruit_BME280.h>
#include <SPI.h>

// BME sensor
#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;

struct bme280Data {
  int32_t temperature;
  uint32_t humidity;
  uint32_t pressure;
  int32_t altitude;
};

bme280Data currentData = bme280Data();
unsigned long lastDataLog = 0;

bme280Data getBME280Data(unsigned long currentMillis) {
  if (currentMillis - lastDataLog < 50 && lastDataLog != 0)
    return currentData;
  lastDataLog = currentMillis;

  // BME
  bme.begin(0x77);

  currentData.altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  currentData.humidity = bme.readHumidity();
  currentData.pressure = bme.readPressure();
  currentData.temperature = bme.readTemperature();

  return currentData;
}
