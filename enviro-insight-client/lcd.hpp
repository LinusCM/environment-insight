#include <LiquidCrystal_I2C.h>

#define I2C_ADDR 0x27
#define LCD_COLS 16
#define LCD_ROWS 2
#define LIGHT A1

LiquidCrystal_I2C lcd(I2C_ADDR, LCD_COLS, LCD_ROWS);

void updateLCD(const char *topText, const char *bottomText) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(topText);
  lcd.setCursor(0, 1);
  lcd.print(bottomText);
}

void updatePage(const char *label, const char *value, const int page,
                const int maxPages) {
  char pageText[8];
  snprintf(pageText, sizeof(pageText), "%d/%d", page + 1, maxPages + 1);
  updateLCD(label, value);
  lcd.setCursor(13, 1);
  lcd.print(pageText);
}

// LCD timeout
uint8_t lastState = 0;
unsigned long lastChangeEpoch = 0;
bool lcdIsOn = false;

bool shouldTurnOnDisplay(unsigned long epoch, uint8_t state) {
  if (lastChangeEpoch == 0 || (!lcdIsOn && state != lastState)) {
    lastState = state;
    lastChangeEpoch = epoch;
    lcdIsOn = true;
    return true;
  }
  return false;
}

bool shouldTurnOffDisplay(unsigned long epoch) {
  if (lcdIsOn && epoch - lastChangeEpoch > 30) {
    lcdIsOn = false;
    return true;
  }
  return false;
}

const char *lightToString(const unsigned int lightValue) {
  if (lightValue < 250) {
    return "Dark";
  } else if (lightValue < 500) {
    return "Dim";
  } else if (lightValue < 800) {
    return "Bright";
  } else {
    return "Very bright";
  }
}
