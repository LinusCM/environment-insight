#include "bme.hpp"
#include "button.hpp"
#include "lcd.hpp"
#include "wifi.hpp"

// Endpoint
const int port = 8080;
const char *server = "13.53.216.32";

// Credentials
const char ssid[] = "Linksys00115";
const char password[] = "Kode1234";

const uint8 maxPages = 3;

int connectionTime = 0;
ButtonHandler buttons = ButtonHandler(buttonOne, buttonTwo, maxPages);
ConnectionHandler *wifi;

void setup() {
  // LCD
  lcd.init();
  lcd.backlight();

  // Serial.begin(9600);
  // while (!Serial)
  //   ;

  // WiFi
  wifi = new ConnectionHandler(ssid, password, server, port);
}

unsigned long epoch;
unsigned long currentMillis;
bool beenOnBattery = false;
char previousBuf[11];
uint8_t previousState = 0;

void loop() {
  // Sending 5v to A0
  int sensorValue = analogRead(A0);

  // Convert the analog reading to voltage
  float voltage = sensorValue * (5.0 / 1023.0);

  if (voltage < 4.5)
    beenOnBattery = true;

  if (beenOnBattery) {
    beenOnBattery = false;

    // LCD
    lcd.init();
    lcd.backlight();
  }

  epoch = wifi->getEpochTimestamp();
  currentMillis = millis();
  bme280Data bmeData = getBME280Data(currentMillis);
  buttons.checkButtons(currentMillis);
  noteState(bmeData, epoch, bme);
  uploadState(epoch, wifi);

  char valueBuf[10];
  char typeBuf[11];

  if (shouldTurnOnDisplay(epoch, buttonState)) {
    lcd.display();
    lcd.backlight();
  }

  if (shouldTurnOffDisplay(epoch)) {
    lcd.noDisplay();
    lcd.noBacklight();
  }

  switch (buttonState) {
  case 0:
    strcpy(typeBuf, "Temperature:");
    strcpy(valueBuf, itoa(bmeData.temperature, valueBuf, 10));
    break;
  case 1:
    strcpy(typeBuf, "Humidity:");
    strcpy(valueBuf, itoa(bmeData.humidity, valueBuf, 10));
    break;
  case 2:
    strcpy(typeBuf, "Light:");
    strcpy(valueBuf, lightToString(analogRead(LIGHT)));
    break;
  case 3:
    strcpy(typeBuf, "Altitude:");
    strcpy(valueBuf, itoa(bmeData.altitude, valueBuf, 10));
    break;
  }

  if (strcmp(valueBuf, previousBuf) != 0 || previousState != buttonState) {
    updatePage(typeBuf, valueBuf, buttonState, maxPages);
    previousState = buttonState;
    strcpy(previousBuf, valueBuf);
  }
}
