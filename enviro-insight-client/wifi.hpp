#include "Adafruit_BME280.h"
#include "MqttClient.h"
#include "WiFiClient.h"
#include "secret.hpp"
#include <ArduinoJson.h>
#include <ArduinoMqttClient.h>
#include <NTPClient.h>
#include <WiFi101.h>
#include <WiFiUdp.h>

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

class ConnectionHandler {
private:
  char ssid[32];
  char password[32];
  char server[64];
  int port;
  WiFiClient client;

  // byte mac[6];

  void connectToWiFi() {
    while (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(ssid, password);
      updateLCD("Connecting to", "WiFi.");
      delay(250);
      updateLCD("Connecting to", "WiFi..");
      delay(250);
      updateLCD("Connecting to", "WiFi...");
      delay(250);
    }
    // WiFi.macAddress(mac);
  }

public:
  ConnectionHandler(const char *ssidInit, const char *passwordInit,
                    const char *serverInit, const int &portInit)
      : port(portInit) {
    strcpy(ssid, ssidInit);
    strcpy(password, passwordInit);
    strcpy(server, serverInit);
    connectToWiFi();
    timeClient.begin();
  }

  void makePostRequest(const char *endpoint,
                       StaticJsonDocument<128> &jsonInput) {
    if (!client.connect(server, port)) {
      return;
    }

    uint8_t jsonLength = measureJson(jsonInput);
    char header[128];
    sprintf(header, "POST %s HTTP/1.1", endpoint);
    client.println(header);
    client.print(F("Host: "));
    client.println(server);
    client.print(F("Content-Length: "));
    client.println(jsonLength);
    client.println(F("Content-Type: application/json"));
    client.println(X_API_HEADER);
    client.println(F("Connection: close"));
    client.println();

    serializeJson(jsonInput, client);

    client.println();

    client.stop();
  }

  unsigned long getEpochTimestamp() {
    timeClient.update();
    return timeClient.getEpochTime();
  }
};

struct sensorState {
  uint16_t light;
  int32_t temperature;
  uint32_t humidity;
  uint32_t pressure;
  int32_t altitude;
};

unsigned long lastUploadEpoch = 0;
unsigned long lastNoteEpoch = 0;

uint8_t updates = 0;
sensorState state = sensorState();
StaticJsonDocument<128> doc;

void noteState(bme280Data bmeData, unsigned long epoch, Adafruit_BME280 bme) {
  if (lastNoteEpoch == epoch || epoch % 60 != 0)
    return;

  int32_t temperature = (int32_t)round(bmeData.temperature);
  uint32_t humidity = (uint32_t)round(bmeData.humidity);
  uint32_t pressure = (uint32_t)round(bmeData.pressure);
  int32_t altitude = (int32_t)round(bmeData.altitude);

  if (altitude > 8800 || temperature > 100)
    return;

  lastNoteEpoch = epoch;
  updates++;

  state.light += analogRead(A1);
  state.temperature += temperature;
  state.humidity += humidity;
  state.pressure += pressure;
  state.altitude += altitude;
}

void uploadState(unsigned long epoch, ConnectionHandler *wifi) {
  if (lastUploadEpoch == epoch || epoch % 1800 != 0 || updates == 0)
    return;
  lastUploadEpoch = epoch;

  doc["client_id"] = "136";
  doc["timestamp"] = epoch;
  doc["light"] = state.light / updates;
  doc["temperature"] = state.temperature / updates;
  doc["humidity"] = state.humidity / updates;
  doc["pressure"] = state.pressure / updates;
  doc["altitude"] = state.altitude / updates;

  wifi->makePostRequest("/api/Data/Save", doc);
  doc.clear();

  updates = 0;
  state = sensorState();
}
